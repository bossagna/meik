<?php

/**
 * Access plugin that provides role-based access control.
 */
class crowdfunding_access_plugin extends views_plugin_access {

  function access($account) {
    return crowdfunding_views_access($this->view, $account);
  }

  function get_access_callback() {
    return array('crowdfunding_views_access', [$this->view]);
  }

}